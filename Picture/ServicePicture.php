<?php

/**
 * This file is part of the Machar libs (https://github.com/Machar)
 *
 * Copyright (c) 2012 Martin Charouzek (martin@charouzkovi.info)
 *
 */

namespace Machar\Mongo;

/**
 *  
 * @author Martin Charouzek <martin@charouzkovi.info>
 * 
 */
final class ServicePicture extends ServiceFile {

	/**
	 * 
	 * @param \Machar\Mongo\Picture $picture
	 * @param string $title
	 * @param boolean $chunkSplit
	 * @return \Nette\Utils\Html
	 */
	public function getHtml(Picture $picture, $title = null, $chunkSplit = true) {
		$path = \Nette\Utils\Strings::match($picture->getSource(), '/\/photo_cache(.*)/');
		$img = \Nette\Utils\Html::el("img");
		$img->src = $path[0];
		$img->title = $title;
		$img->alt = $title;
		return $img;
	}

	public function saveToDisk(Picture $picture, $width = 0, $height = 0) {
		$file = WWW_DIR . "/photo_cache/" . $picture->getIdMongo() . "-" . $width . "-" . $height . ".jpg";
		if (!file_exists(WWW_DIR . '/photo_cache')) {
			@mkdir(WWW_DIR . '/photo_cache', 0777, true);
		}
		$image = \Nette\Image::fromString(base64_decode($picture->getSource()));
		$image->save($file, 80, \Nette\Image::JPEG);
	}

	/**
	 * 
	 * @param string $idMongo
	 * @param int $width
	 * @param int $height
	 * @return \Picture|null
	 */
	public function get($mongoId, $width = 0, $height = 0) {
		$file = WWW_DIR . "/photo_cache/" . $mongoId . "-" . $width . "-" . $height . ".jpg";
		if (!file_exists(WWW_DIR . '/photo_cache')) {
			@mkdir(WWW_DIR . '/photo_cache', 0777, true);
		}
		if (!file_exists($file)) {
			$picture = null;
			if ($width == 0 AND $height == 0) {
				$resource = $this->getFullSource($mongoId);
			} else {
				$resolution = $width . "_" . $height;
				$columns = array($resolution, "insertDate");
				$resource = $this->mongo->select($this->table, $columns)->where(array("_id" => new \MongoId($mongoId)))->fetch();
				if ($resource) {
					if (!isset($resource[$resolution])) {
						$resource = $this->getFullSource($mongoId);
						if ($resource) {
							$img = \Nette\Image::fromString(base64_decode($resource["source"]));
							if ($width == 0 OR $height == 0) {
								if ($width == 0 AND $height != 0) {
									$img->resize(null, $height);
								} elseif ($width != 0 AND $height == 0) {
									$img->resize($width, null);
								}
							} else {
								if ($img->getWidth() >= $img->getHeight()) {
									$img->resize(null, $height);
									$img->crop(($img->getWidth() - $width) / 2, 0, $width, $height);
								} else {
									$img->resize($width, null);
									$img->crop(0, ($img->getHeight() - $height) / 2, $width, $height);
								}
							}
							$img->antialias(true);
							$img->sharpen();
							$resource["width"] = $width;
							$resource["height"] = $height;
							$resource["source"] = base64_encode($img->toString(\Nette\Image::JPEG, 90));
							$this->mongo->update($this->table, array("_id" => new \MongoId($mongoId)), array($resolution => $resource["source"]))->exec();
						}
					} else {
						$resource["source"] = $resource[$resolution];
						$resource["width"] = $width;
						$resource["height"] = $height;
						unset($resource[$resolution]);
					}
				}
			}
			if ($resource) {
				$picture = new Picture();
				$picture->setIdMongo((string) $resource["_id"]);
				$picture->setSource($resource["source"]);
				$picture->setWidth($resource["width"]);
				$picture->setHeight($resource["height"]);
				$picture->setInsertDate($resource["insertDate"]);
			}
			if ($picture instanceof Picture) {
				$this->saveToDisk($picture, $width, $height);
				$picture->setSource($file);
			}
		} else {
			$picture = new Picture();
			$picture->setIdMongo($mongoId);
			$picture->setSource($file);
			if ($width != 0)
				$picture->setWidth($width);
			if ($height != 0)
				$picture->setHeight($height);
		}
		return $picture;
	}

	/**
	 * 
	 * @param string $mongoId
	 * @return array|null
	 */
	private function getFullSource($mongoId) {
		$columns = array("width", "height", "source", "insertDate");
		$args = array("_id" => new \MongoId($mongoId));
		$resource = $this->mongo->select($this->table, $columns)->where($args)->fetch();
		return $resource;
	}

	/**
	 * 
	 * @param Picture $picture
	 * @return string $mongoId
	 */
	public function update(File $picture) {
		$list = array();
		$list['width'] = $picture->getWidth();
		$list['height'] = $picture->getHeight();
		$list['source'] = $picture->getSource();
		$list['insertDate'] = $picture->getInsertDate();
		return $this->mongo->update($this->table, array("_id" => new \MongoId($picture->getIdMongo())), $list)->exec();
	}

	/**
	 * 
	 * @param Picture $picture
	 * @return string $mongoId
	 */
	public function save(File $picture, array $args = array()) {
		$list = array();
		$list['width'] = $picture->getWidth();
		$list['height'] = $picture->getHeight();
		$list['source'] = $picture->getSource();
		$list['insertDate'] = $picture->getInsertDate();
		foreach ($args as $key => $value) {
			$list[$key] = $value;
		}
		return $this->mongo->insert($this->table, $list)->exec();
	}

}

?>
