<?php

/**
 * This file is part of the Machar libs (https://github.com/Machar)
 *
 * Copyright (c) 2012 Martin Charouzek (martin@charouzkovi.info)
 *
 */

namespace Machar\Mongo;

use Nette;
use Nette\Diagnostics\Debugger;

/**
 *  
 * @author Martin Charouzek <martin@charouzkovi.info>
 * @method send(string $cmd, string $table, string $mongoId, array $args) Send command to MongoDB ($cmd: GET|UPDATE|SAVE|DELETE)
 * 
 */
class MongoClient extends Nette\Object {

    const GET = "select",
            UPDATE = "update",
            SAVE = "insert",
            DELETE = "remove";

    /**
     * @var \MongoDB
     */
    private $mongoDB;

    /**
     * @var Diagnostics\Panel
     */
    private $panel;

    /**
     * @var string $host
     */
    private $host;

    /**
     * @var string $port
     */
    private $port;

    /**
     * @var strin $database
     */
    private $database;

    /**
     *
     * @var string $table
     */
    private $table;
    /**
     *
     * @var string $command
     */
    private $command;
    /**
     *
     * @var array $columns
     */
    private $columns = array();
    /**
     *
     * @var array $args
     */
    private $args = array();
    /**
     *
     * @var int $limit
     */
    private $limit = 0;
    /**
     *
     * @var int $offset
     */
    private $offset = 0;
    /**
     *
     * @var array $sort
     */
    private $sort = array("_id" => -1);

    /**
     * 
     * @param string $host
     * @param int $port
     * @param string $database
     */
    public function __construct($host = 'localhost', $port = 27017, $database = 0) {
        $this->host = $host;
        $this->port = $port;
        $this->database = $database;
    }

    /**
     * 
     * @return MongoClient
     * @throws MongoClientException
     */
    private function connect() {
        if ($this->mongoDB instanceof \MongoDB) {
            return;
        }
        $mongo = new \Mongo("mongodb://" . $this->host . ":" . $this->port, array("timeout" => 20));
        $this->mongoDB = $mongo->{$this->database};
        if (!$this->mongoDB) {
            throw new MongoClientException('Cannot connect to mongo server: ' . $errstr, $errno);
        }
    }

    /**
     * Close the connection
     */
    private function close() {
        @fclose($this->mongoDB);
        $this->mongoDB = FALSE;
    }

    /**
     * 
     * @param array $columns
     * @return \Machar\Mongo\MongoClient
     */
    public function &select($table, array $columns = array()) {
        $this->table = $table;
        $this->command = self::GET;
        $this->columns = $columns;
        return $this;
    }

    /**
     * 
     * @param string $table
     * @return \Machar\Mongo\MongoClient
     */
    public function &delete($table) {
        $this->table = $table;
        $this->command = self::DELETE;
        return $this;
    }

    /**
     * 
     * @param string $table
     * @param array $list
     * @return \Machar\Mongo\MongoClient
     */
    public function &insert($table, $list) {
        $this->table = $table;
        $this->columns = $list;
        $this->command = self::SAVE;
        return $this;
    }

    /**
     * 
     * @param string $table
     * @param array $args update on which record $key=>$value
     * @param array $list list of changes $key => $value
     * @return \Machar\Mongo\MongoClient
     */
    public function &update($table, $args, $list) {
        $this->table = $table;
        $this->args = $args;
        $this->columns = $list;
        $this->command = self::UPDATE;
        return $this;
    }
    
    /**
     * 
     * @param array $args
     * @return \Machar\Mongo\MongoClient
     */
    public function &where(array $args) {
        $this->args = $args;
        return $this;
    }

    /**
     * 
     * @param int $limit
     * @return \Machar\Mongo\MongoClient
     */
    public function &limit($limit) {
        $this->limit = $limit;
        return $this;
    }

    /**
     * 
     * @param int $offset
     * @return \Machar\Mongo\MongoClient
     */
    public function &offset($offset) {
        $this->offset = $offset;
        return $this;
    }

    /**
     * 
     * @return FALSE|TRUE|array|$mongoId
     * @throws MongoClientException
     */
    public function fetch() {
        if ($this->command !== self::GET) {
            throw new MongoClientException("Cannot fetch on " . $this->command . " command, use exec() function");
        }
        $this->limit = 1;
        return $this->sendCommand();
    }

    /**
     * 
     * @return FALSE|TRUE|array|$mongoId
     * @throws MongoClientException
     */
    public function fetchAll() {
        if ($this->command !== self::GET) {
            throw new MongoClientException("Cannot fetch on " . $this->command . " command, use exec() function");
        }
        return $this->sendCommand();
    }

    /**
     * 
     * @return FALSE|TRUE|array|$mongoId
     */
    public function exec() {
        return $this->sendCommand();
    }

    /**
     * 
     * @return FALSE|TRUE|array|$mongoId
     * @throws MongoClientException
     */
    private function sendCommand() {

        if ($this->table === null) {
            throw new MongoClientException("Specify mongo TABLE with argument FROM");
        }

        $this->connect();

        if ($this->panel) {
            $request = $this->command . " " . $this->table;
            $this->panel->begin($request);
        }

        $result = FALSE;
        try {
            switch ($this->command) {
                case "select":
                    $cursor = $this->mongoDB->{$this->table}->find($this->args, $this->columns)->limit($this->limit)->skip($this->offset)->sort($this->sort);
                    if ($this->limit == 1) {
                        if ($cursor->hasNext()) {
                            $result = $cursor->getNext();
                        }
                    } else {
                        $list = array();
                        foreach ($cursor as $fetch) {
                            $list[] = $fetch;
                        }
                        $result = $list;
                    }
                    break;
                case "remove":
                    $this->mongoDB->{$this->table}->remove($this->args);
                    $result = TRUE;
                    break;
                case "insert":
                    $data = $this->mongoDB->{$this->table}->insert($this->columns);
                    $result = (string) $this->columns["_id"];
                    break;
                case "update":
                    $this->mongoDB->{$this->table}->update($this->args, array('$set' => $this->columns));
                    $result = TRUE;
                    break;
                default:
                    throw new MongoClientException("No valid command set");
                    break;
            }
        } catch (\MongoException $e) {
            Debugger::log($e->getMessage(), "mongo");
            throw new MongoClientException("Mongo disconnect");
            if ($this->panel) {
                $this->panel->error($e);
            }
        }
        if ($this->panel) {
            $this->panel->end();
        }

        return $result;
    }

    /**
     * @param Diagnostics\Panel $panel
     */
    public function setPanel(Diagnostics\Panel $panel) {
        $this->panel = $panel;
    }

    /**
     * Close the connection
     */
    public function __destruct() {
        $this->close();
    }

}

/**
 * @author Martin Charouzek <martin@charouzkovi.info>
 */
class MongoClientException extends \Exception {
    
}
