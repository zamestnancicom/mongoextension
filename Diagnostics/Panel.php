<?php

/**
 * This file is part of the Machar libs (https://github.com/Machar)
 *
 * Copyright (c) 2012 Martin Charouzek (martin@charouzkovi.info)
 *
 */

namespace Machar\Mongo\Diagnostics;

use Nette;
use Nette\Diagnostics\Debugger;
use Nette\Utils\PhpGenerator as Code;

/**
 * @author Martin Charouzek <martin@charouzkovi.info>
 */
class Panel extends Nette\Object implements \Nette\Diagnostics\IBarPanel {
    /** @internal */

    const TIMER_NAME = 'mongo-client-timer';

    /**
     * @var int
     */
    public static $maxLength = 1000;

    /**
     * @var int
     */
    private $totalTime = 0;

    /**
     * @var array
     */
    private $queries = array();

    /**
     * @var int
     */
    private $transfered = 0;

    /**
     * @var array
     */
    private $errors = array();

    /**
     */
    public function begin($cmd) {
        Debugger::timer(self::TIMER_NAME);
        $this->queries[] = (object) array(
                    'errors' => array(),
                    'cmd' => implode(' ', (array) $cmd),
                    'size' => 0,
                    'time' => 0
        );
    }

    /**
     * @param \Exception $e
     */
    public function error(\Exception $e) {
        $this->errors[] = $e;
        if ($query = end($this->queries)) {
            $query->errors[] = $e;
        }
    }

    /**
     * @param string $size
     */
    public function dataSize($size) {
        $this->transfered += $size;
        if ($query = end($this->queries)) {
            $query->size += $size;
        }
    }

    /**
     */
    public function end() {
        $time = Debugger::timer(self::TIMER_NAME);
        if ($query = end($this->queries)) {
            $query->time = $time;
        }
        $this->totalTime += $time;
    }

    /**
     * Renders HTML code for custom tab.
     * @return string
     */
    public function getTab() {
        return '<span title="Mongo">' .
                '<img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAcpJREFUeNpsk81qVEEQhb/q23NzM8nEgQkk6CJE3AiCQgZ8CPEBfAVfzY1PoeDGhRsFyVIEE0OScXJ/uuu4mGQyMzcNtemf79Spqrbp8f4pMMaMzeVdxsygCA8dI3QRgX1gtLILQL5J7L48oFUmfTuHqugTsCIAqUduMuXTMYfvp7RvH5MPSiwJzNbCjBR6j11QFVTvnuGVkYeB+ZtDtBUwgZktA6AHoHX8ZEI+2qa5abDG6Z5U1K/2oHV0a3EBsA2ABNG4eTGibTs6z2Q5ap36+QhVAbsjIEArAAEOeTcynwxo2kSXEy6h5KRHA9KkhKylFtq04OBVoC2hzZnkGXctylyA7xaYr+gbxPWuAC6SZ2rPNJ6RCyFw3ctK3I3FegYB4iwTrjpaE61nXCJL5M6xq4TC0v6GBQOCUcyd4fcZTRR1TmR3PBjxT8vgvEPBkC3Sf7CNisb48yXFWUNdLOxgsPP1CpIg2FJdD85BNOJlYv/DL+JcjMohwy8XbP/4BwO7r8PtzPcBBtoKlD9nHH38y+vTPXY+naHy1uZqABEUF6+0Vg7FQPG7JlQ12vhoa/lOjyenYONeLSSKGKmqitn1NRYC1rvDxf8BAGot7UH6J8mVAAAAAElFTkSuQmCC" />' .
                count($this->queries) . ' queries' .
                ($this->errors ? ' / ' . count($this->errors) . ' errors' : '') .
                ($this->transfered ? ' / ' . number_format($this->transfered / 1000000, 2, '.', ' ') . ' MB' : '') .
                ($this->queries ? ' / ' . sprintf('%0.1f', $this->totalTime * 1000) . ' ms' : '') .
                '</span>';
    }

    /**
     * Renders HTML code for custom panel.
     * @return string
     */
    public function getPanel() {
        $s = '';
        $h = 'htmlSpecialChars';
        foreach ($this->queries as $query) {
            $s .= '<tr><td>' . sprintf('%0.3f', $query->time * 1000000);
            $s .= '</td><td>' . number_format($query->size / 1000, 3, '.', ' ');
            $s .= '</td><td class="kdyby-RedisClientPanel-cmd">' .
                    $h(self::$maxLength ? Nette\Utils\Strings::truncate($query->cmd, self::$maxLength) : $query->cmd);
            $s .= '</td></tr>';
        }

        return empty($this->queries) ? '' :
                '<style> #nette-debug div.kdyby-RedisClientPanel table td { text-align: right }
			#nette-debug div.kdyby-RedisClientPanel table td.kdyby-RedisClientPanel-cmd { background: white !important; text-align: left } </style>
			<h1>Queries: ' . count($this->queries) . ($this->totalTime ? ', time: ' . sprintf('%0.3f', $this->totalTime * 1000) . ' ms' : '') . '</h1>
			<div class="nette-inner kdyby-RedisClientPanel">
			<table>
				<tr><th>Time&nbsp;µs</th><th>Response&nbsp;KB</th><th>Command</th></tr>' . $s . '
			</table>
			</div>';
    }

    /**
     * @param \Exception|MongoClientException $e
     *
     * @return array
     */
    public static function renderException($e) {
        if ($e instanceof MongoClientException) {
            $panel = NULL;
            if ($e->request) {
                $panel .= '<h3>Mongo Request</h3>' .
                        '<pre class="nette-dump"><span class="php-string">' .
                        nl2br(htmlSpecialChars(implode(' ', $e->request))) .
                        '</span></pre>';
            }
            if ($e->response) {
                $response = Code\Helpers::dump($e->response);
                $panel .= '<h3>Mongo Response (' . strlen($e->response) . ')</h3>' .
                        '<pre class="nette-dump"><span class="php-string">' .
                        htmlSpecialChars($response) .
                        '</span></pre>';
            }

            if ($panel !== NULL) {
                $panel = array(
                    'tab' => 'Mongo Response',
                    'panel' => $panel
                );
            }

            return $panel;
        }
    }

    /**
     * @return \Machar\Mongo\Diagnostics\Panel
     */
    public static function register() {
        Debugger::$blueScreen->addPanel(array($panel = new static(), 'renderException'));
        Debugger::$bar->addPanel($panel);
        return $panel;
    }

}
