<?php

/**
 * This file is part of the Machar libs (https://github.com/Machar)
 *
 * Copyright (c) 2012 Martin Charouzek (martin@charouzkovi.info)
 *
 */

namespace Machar\Mongo;

/**
 *  
 * @author Martin Charouzek <martin@charouzkovi.info>
 * 
 */
class File extends \Nette\Object {

    /**
     *
     * @var int $idFile
     */
    protected $idMongo;
    /**
     *
     * @var base64_encoded $source
     */
    protected $source;
    /**
     *
     * @var timestamp $insertDate
     */
    protected $insertDate;

    /**
     * 
     * @param mixed $resource
     * @return \static File
     */
    public static function fromString($resource) {
        $type = \Nette\Utils\MimeTypeDetector::fromString($resource);
        $file = new static;
        $file->setSource(base64_encode($resource));
        $file->setInsertDate(time());
        return $file;
    }

    public function fromFile($filepath) {
        
    }

    /**
     * 
     * @param string $mimeType
     * @return string $extension
     */
    public static function getExtensionFromMimeType($mimeType) {
        switch ($mimeType) {
            case "text/plain":
                return "txt";
            case "image/jpeg":
                return "jpg";
            case "image/png":
                return "png";
            case "application/pdf":
                return "pdf";
            default:
                return "txt";
        }
    }

    public function getIdMongo() {
        return new \MongoId($this->idMongo);
    }

    public function setIdMongo($idMongo) {
        $this->idMongo = $idMongo;
    }

    public function getSource() {
        return $this->source;
    }

    public function setSource($source) {
        $this->source = $source;
    }

    /**
     * 
     * @return string $mimeType
     */
    public function getMimeType() {
        return \Nette\Utils\MimeTypeDetector::fromString(base64_decode($this->source));
    }

    /**
     * 
     * @return string $extension
     */
    public function getExtension() {
        return File::getExtensionFromMimeType($this->getMimeType());
    }

    public function getInsertDate() {
        return $this->insertDate;
    }

    public function setInsertDate($insertDate) {
        $this->insertDate = $insertDate;
    }

}

?>
